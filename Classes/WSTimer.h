//
//  WSTimer.h
//  007---Dispatch_source
//
//  Created by zhangcong on 2019/3/14.
//  Copyright © 2019 zhangcong. All rights reserved.
//

#import <Foundation/Foundation.h>
@class WSTimer;
typedef void(^TimerBlock) (WSTimer * timer);

@interface WSTimer : NSObject


/**
 初始化定时器，会自动启动

 @param interval 时间间隔
 @param repeats 是否重复
 @param block 回调
 @return timer
 */
+ (WSTimer *)scheduledTimerWithTimeInterval:(NSTimeInterval)interval repeats:(BOOL)repeats block:(TimerBlock)block;


/**
 初始化定时器，不会自动启动

 @param interval 时间间隔
 @param repeats 是否重复
 @param block 回调
 @return timer
 */
+ (WSTimer *)timerWithTimeInterval:(NSTimeInterval)interval repeats:(BOOL)repeats block:(TimerBlock)block;

/**
 启动定时器
 */
- (void)start;


/**
 暂停定时器
 */
- (void)suspend;


/**
 停止定时器
 */
- (void)stop;


@end

